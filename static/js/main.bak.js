Vue.filter('animateStyle', function(animate) {
  return {
    animationName: animate.name,
    animationDuration: animate.duration + 's',
    animationDelay: animate.delay + 's'
  };
});
Vue.filter('postionStyle', function(position) {
  return {
    top: position.top * h + 'px',
    left: position.left * w + 'px',
    width: position.width * w + 'px',
    height: position.height * h + 'px',
    transform: "rotate(" + position.transform + "deg)"
  };
});
var animationControl = {
  initAnimationItems: function() {
    $('.animated').each(function() {
      var aniDuration, aniDelay;
      $(this).attr('data-origin-class', $(this).attr('class'));
      aniDuration = $(this).data('ani-duration');
      aniDelay = $(this).data('ani-delay');
      $(this).css({
        'visibility': 'hidden',
        'animation-duration': aniDuration,
        '-webkit-animation-duration': aniDuration,
        'animation-delay': aniDelay,
        '-webkit-animation-delay': aniDelay
      });
    });
  },
  playAnimation: function(swiper) {
    this.clearAnimation();
    var aniItems = swiper.slides[swiper.activeIndex].querySelectorAll('.animated');
    $(aniItems).each(function() {
      var aniName;
      $(this).css({ 'visibility': 'visible' });
      aniName = $(this).data('ani-name');
      $(this).addClass(aniName);
    });
  },
  clearAnimation: function() {
    $('.animated').each(function() {
      $(this).css({ 'visibility': 'hidden' });
      $(this).attr('class', $(this).data('origin-class'));
    });
  }
};
var w = $(window).width() / 320;
var h = $(window).height() / 488;
var app = new Vue({
  el: 'body',
  data: {
    pages: []
  },
  created: function() {
    var _this = this;
    $.getJSON('/data_01.json', function(data) {
      _this.pages = data.slide.pages;
    });
  },
  attached: function() {
    setTimeout(function() {
      alert('01');
      new Swiper('.swiper-container', {
        mousewheelControl: false,
        effect: 'slide',    // slide, fade, coverflow or flip
        speed: 400,
        direction: 'vertical',
        fade: {
            crossFade: false
        },
        coverflow: {
            rotate: 100,
            stretch: 0,
            depth: 300,
            modifier: 1,
            slideShadows: false     // do disable shadows for better performance
        },
        flip: {
            limitRotation: true,
            slideShadows: false     // do disable shadows for better performance
        },
        onInit: function(swiper) {
          alert('ok');
          // animationControl.initAnimationItems();
          // animationControl.playAnimation(swiper);
        },
        onTransitionStart: function(swiper) { // on the last slide, hide .btn-swipe
          if (swiper.activeIndex === swiper.slides.length - 1) {
            $upArrow.hide();
          } else {
            $upArrow.show();
          }
        },
        onTransitionEnd: function(swiper) { // play animations of the current slide
          // animationControl.playAnimation(swiper);
        },
        onTouchStart: function(swiper, event) { // mobile devices don't allow audios to play automatically, it has to be triggered by a user event(click / touch).
          if (!$btnMusic.hasClass('paused') && bgMusic.paused) {
            bgMusic.play();
          }
        }
      });
      $('.loading-overlay').slideUp();
    }, 0)
  },
  components: {
    hyText: {
      template: '#hyText',
      props: ['comp']
    },
    hyImage: {
      template: '#hyImage',
      props: ['comp']
    },
    hyForm: {
      template: '#hyForm',
      props: ['comp']
    }
  }
});

var bgMusic = $('audio').get(0);
var $btnMusic = $('.btn-music');
var $upArrow = $('.up-arrow');
$btnMusic.click(function() {
  if (bgMusic.paused) {
    bgMusic.play();
    $(this).removeClass('paused');
  } else {
    bgMusic.pause();
    $(this).addClass('paused');
  }
});