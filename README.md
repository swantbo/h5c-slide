# h5c-slide

[h5c](https://gitee.com/yangjunjun/h5c) 项目产生的 json, 通过本项目来展示为一个可以通过浏览器访问的 h5 页面， [在线示例](https://yangjunye.gitee.io/h5c-slide)(请调整为手机模式)

## 截图

![screenshot](screenshot.png)
