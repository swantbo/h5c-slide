'use strict';

var autoprefixer = require('gulp-autoprefixer');
var browserify = require('gulp-browserify');
var browserSync = require('browser-sync').create();
var concat = require('gulp-concat');
var del = require('del');
var gulp = require('gulp');
var imagemin = require('gulp-imagemin');
var inject = require('gulp-inject');
var minifycss = require('gulp-minify-css');
var notify = require('gulp-notify');
var rename = require('gulp-rename');
var runSequence = require('run-sequence');
var sass = require('gulp-sass');
var streamSeries = require('stream-series');
var plumber = require('gulp-plumber');
var uglify = require('gulp-uglify');


var resource = {
  css: ['static/css/animate.min.css',
    'static/css/swiper.min.css',
    'static/css/main.css'
  ],
  js: ['static/js/jquery.min.js',
      'static/js/vue.min.js',
      'static/js/swiper.min.js',
      'static/js/main.js']
}


/* ============================================================================================================
============================================ For Development ==================================================
=============================================================================================================*/

gulp.task('css', function() {
  var css = resource.css;

  return gulp.src('static/css/main.css')
    .pipe(autoprefixer())
    .pipe(gulp.dest('static/css/'));
});

gulp.task('publish-css', function() {
  var css = resource.css;
  return gulp.src(css)
    .pipe(autoprefixer())
    .pipe(concat('bundle.css'))
    .pipe(gulp.dest('dist/css'))
    .pipe(minifycss())
    .pipe(rename({
      suffix: '.min'
    }))
    .pipe(gulp.dest('dist/css'));
});
gulp.task('publish-js', function() {
  var js = resource.js;
  return gulp.src(js)
    .pipe(concat('bundle.js'))
    .pipe(gulp.dest('dist/js'))
    .pipe(uglify())
    .pipe(rename({
      suffix: '.min'
    }))
    .pipe(gulp.dest('dist/js'));
});